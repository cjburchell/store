namespace Store.Services
{
    /// <summary>
    /// Item in stock
    /// </summary>
    public class StockListing
    {
        /// <summary>
        /// Name of the item
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// Id of the item
        /// </summary>
        public string Id { get; set; }
        
        /// <summary>
        /// Description of the items
        /// </summary>
        public string Description { get; set; }
        
        /// <summary>
        /// Number of items in stock
        /// </summary>
        public int Count { get; set; }
    }
}