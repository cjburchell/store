using System.Collections.Generic;

namespace Store.Services
{
    /// <summary>
    /// Database object
    /// </summary>
    internal class Inventory{
        public List<StockLocation> Shops { get; set; }
        public List<StockLocation> Warehouses { get; set; }
    }
}