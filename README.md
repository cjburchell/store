# Store - ASP.NET Core 2.0 Server

A simple store API

## Run

Linux/OS X:

```
sh build.sh
```

Windows:

```
build.bat
```

## Run in Docker

```
docker build -t store .
docker run -p 8080:8080 store
```
