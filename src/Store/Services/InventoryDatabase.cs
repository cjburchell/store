using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;

namespace Store.Services
{
    internal class InventoryDatabase : IInventoryDatabase
    {
        private readonly Inventory _inventory;

        public InventoryDatabase()
        {
            var dbFile = Environment.GetEnvironmentVariable("STORE_DB_PATH");
            if (dbFile == null)
            {
                this._inventory = new Inventory
                {
                    Shops = new List<StockLocation>(),
                    Warehouses = new List<StockLocation>()
                };
            }
            else
            {
                this._inventory = JsonConvert.DeserializeObject<Inventory>(File.ReadAllText(dbFile));
            }
            
        }
        
        public IEnumerable<StockLocation> GetShops()
        {
            return this._inventory.Shops;
        }

        public StockLocation GetShop(string id)
        {
            return this._inventory.Shops.FirstOrDefault(item => item.Id == id);
        }

        public IEnumerable<StockLocation> GetWarehouses()
        {
            return this._inventory.Warehouses;
        }

        public StockLocation GetWarehouse(string id)
        {
            return this._inventory.Warehouses.FirstOrDefault(item => item.Id == id);
        }

        public bool MoveStock(string from, string to, string id, int count)
        {
            var warehouse = this._inventory.Warehouses.FirstOrDefault(item => item.Id == id);
            if (warehouse == null)
            {
                return false;
            }
            
            var store = this._inventory.Shops.FirstOrDefault(item => item.Id == id);
            if (store == null)
            {
                return false;
            }
            
            var stockItem = warehouse.Stock.FirstOrDefault(item => item.Id == id);
            if (stockItem == null)
            {
                return false;
            }

            if (stockItem.Count < count)
            {
                return false;
            }
            
            var storeItem = store.Stock.FirstOrDefault(item => item.Id == id);
            if (storeItem == null)
            {
                stockItem.Count -= count;
                storeItem = new StockListing
                {
                    Name = stockItem.Name,
                    Id = stockItem.Id,
                    Description = stockItem.Description,
                    Count = count,
                };
                store.Stock.Add(storeItem);
                return true;
            }

            stockItem.Count -= count;
            storeItem.Count += count;
            return true;

        }
    }
}