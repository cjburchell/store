using System.Collections.Generic;

namespace Store.Services
{
    /// <summary>
    /// Store Interface
    /// </summary>
    public interface IInventoryDatabase
    {
        /// <summary>
        /// Get a list of shops
        /// </summary>
        /// <returns>list of shops</returns>
        IEnumerable<StockLocation> GetShops();
        
        /// <summary>
        /// Get a shop
        /// </summary>
        /// <param name="id">Id of the shop</param>
        /// <returns>the requested shop</returns>
        StockLocation GetShop(string id);
        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerable<StockLocation> GetWarehouses();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        StockLocation GetWarehouse(string id);

        /// <summary>
        /// Moves stock from warehouse to store
        /// </summary>
        /// <param name="from">Id of the warehouse</param>
        /// <param name="to">Id of the shop</param>
        /// <param name="id">Id of the item</param>
        /// <param name="count">number of items to move</param>
        /// <returns></returns>
        bool MoveStock(string from, string to, string id, int count);
    }
}   